# Simple Text Editor for Android

This is a fork of [TextPad](https://github.com/maxistar/TextPad) modified to use a more recent version of Gradle so that it can be built in [AndroidIDE](https://androidide.com).

It only supports editing plaintext files. You can edit Markdown / Zim Wiki files, but there won't be any syntax highlighting.

If you don't care about AndroidIDE compiler support, you can skip to downloading the binaries from [F-Droid](http://fdroidorg6cooksyluodepej4erfctzk7rrjpjbbr6wx24jh3lqyfwyd.onion/en/packages/com.maxistar.textpad/).
